#!/bin/bash
filename=xampp-linux-x64-7.3.8-2-installer.run
rm $filename > /dev/null
#download
wget https://www.apachefriends.org/xampp-files/7.3.8/$filename 
chmod +x $filename

#install
./$filename << OPTION
Y
Y
Y

Y

OPTION

#setup directories 
sudo chown ec2-user:ec2-user /opt/lampp/htdocs
ln -s /opt/lampp/htdocs web
cat > web/test.html << FILE
Test string for index page
FILE

#start services
/opt/lampp/xampp start
#incase it's already started
/opt/lampp/xampp restart
#clean up
rm $filename
rm new.conf
