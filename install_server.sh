
filename=xampp-linux-x64-7.3.8-2-installer.run
rm $filename > /dev/null
#download
wget https://www.apachefriends.org/xampp-files/7.3.8/$filename 
chmod +x $filename

#install
./$filename << OPTION
Y
Y
Y

Y

OPTION

#start services
/opt/lampp/xampp start
#incase it's already started
/opt/lampp/xampp restart
#clean up
rm $filename

#setup automatic startup on boot
echo test message > /opt/lampp/htdocs/test.html
cp /opt/lampp/lampp .
sed -i '/# chkconfig:/ c# chkconfig: 2345 80 30' lampp
cp lampp /etc/init.d
chkconfig --add lampp
rm lampp
